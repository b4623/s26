const http = require('http')
const port = 3000;
const server = http.createServer((request, response) => {
			if(request.url== "/loginpage") {
				response.writeHead(200, {"Content-Type": 'text/plain'});
				response.end("This is the Log In Page!")
			}
			else{
				response.writeHead(404, {"Content-Type": 'text/plain'});
				response.end("Page Not Found")
			}

})


server.listen(port);

console.log(`Server now running at localhost: ${port}.`);